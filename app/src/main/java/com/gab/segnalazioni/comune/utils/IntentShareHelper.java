package com.gab.segnalazioni.comune.utils;

import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by Gabriele Coppola on 10/03/2018.
 */

public class IntentShareHelper {

    public static void shareReport(AppCompatActivity appCompatActivity, String textBody, Uri fileUri) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");

        //Questo testo sará comunque ignorato da Facebook perché per le loro policy non accettano
        //il pre-filling del testo senza che si usi il loro SDK specifico
        intent.putExtra(Intent.EXTRA_TEXT, !TextUtils.isEmpty(textBody) ? textBody : "");

        if (fileUri != null) {
            intent.putExtra(Intent.EXTRA_STREAM, fileUri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setType("image/*");
        }

        appCompatActivity.startActivity(intent);
    }

}
