package com.gab.segnalazioni.comune.ui.home;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.gab.segnalazioni.comune.BuildConfig;
import com.gab.segnalazioni.comune.R;
import com.gab.segnalazioni.comune.ui.reports.ReportListActivity;
import com.gab.segnalazioni.comune.utils.FileUtils;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;


public class ChoiceFragment extends Fragment {

    public static final int PERMISSION_REQUEST_CAMERA = 10;
    public static final int PERMISSION_REQUEST_STORAGE = 11;

    public static final int REQUEST_PIC_FROM_GALLERY = 20;
    public static final int REQUEST_PIC_FROM_CAMERA = 21;
    public static final int AUTOCOMPLETE_REQUEST_CODE = 1;

    public static ChoiceFragment newInstance(){
        return new ChoiceFragment();
    }

    private FragmentActivity mActivity;

    @BindView(R.id.option_frag_take_open_gallery)
    AppCompatButton openGalleryBtn;

    @BindView(R.id.option_frag_take_photo_btn)
    AppCompatButton takePictureBtn;

    @BindView(R.id.option_frag_take_open_sent_items)
    AppCompatButton openSentItemsBtn;

    private Unbinder unbinder;

    private File photoFile;
    private Uri selectedPhoto;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_choice, container, false);
        unbinder = ButterKnife.bind(this, view);

        openGalleryBtn.setOnClickListener(v -> {
            int wesPermission = ContextCompat.checkSelfPermission(mActivity,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (wesPermission != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(mActivity,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSION_REQUEST_STORAGE);
            } else {
                pickPhotoFromGallery();
            }
        });

        takePictureBtn.setOnClickListener(v -> {
            int cameraPermission = ContextCompat.checkSelfPermission(mActivity,
                    Manifest.permission.CAMERA);
            if (cameraPermission != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(mActivity,
                        new String[]{Manifest.permission.CAMERA},
                        PERMISSION_REQUEST_CAMERA);
            } else {
                pickPhotoFromCamera();
            }
        });

        openSentItemsBtn.setOnClickListener(v -> {
            Intent intent = new Intent(mActivity, ReportListActivity.class);
            startActivity(intent);
        });

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickPhotoFromGallery();
                } else {
                    //TODO permission not granted, handle in some way
                }
                break;
            }
            case PERMISSION_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickPhotoFromCamera();
                } else {
                    //TODO permission not granted, handle in some way
                }
                break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case REQUEST_PIC_FROM_CAMERA: {
                if (resultCode == RESULT_OK) {
                    //add pic to gallery
                    galleryAddPic();
                    selectedPhoto = Uri.fromFile(photoFile);
                }
                break;
            }
            case REQUEST_PIC_FROM_GALLERY: {
                if (resultCode == RESULT_OK) {
                    try {
                        File copiedFile = FileUtils.createImageFile(mActivity);

                        InputStream inputStream = mActivity.getContentResolver()
                                .openInputStream(data.getData());
                        FileOutputStream fileOutputStream = new FileOutputStream(copiedFile);
                        FileUtils.copyStream(inputStream, fileOutputStream);
                        fileOutputStream.close();
                        inputStream.close();

                        selectedPhoto = Uri.fromFile(copiedFile);
                    } catch (Exception e){
                        e.printStackTrace();
                        return;
                    }
                }
                break;
            }
            case AUTOCOMPLETE_REQUEST_CODE: {
                if (resultCode == RESULT_OK) {
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    //Andiamo nel prossimo Fragment con l'indirizzo selezionato
                    mActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.container,
                                SendFragment.newInstance(selectedPhoto, place.getAddress()))
                        .commit();
                    return;
                } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                    Status status = Autocomplete.getStatusFromIntent(data);
                    Log.i("ERROR", status.getStatusMessage());
                }
            }
        }

        if (selectedPhoto != null && !(resultCode == RESULT_CANCELED)){
            Log.e("URI_IMAGE", selectedPhoto.toString());
            openFindAddressActivity();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (FragmentActivity) context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void pickPhotoFromCamera(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            photoFile = FileUtils.createImageFile(mActivity);
        } catch (IOException ex) {
            // Error occurred while creating the File
        }
        // Continue only if the File was successfully created
        if (photoFile != null) {
            Uri photoURI = FileProvider.getUriForFile(mActivity,
                    BuildConfig.FILE_PROVIDER_AUTHORITY,
                    photoFile);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(takePictureIntent, REQUEST_PIC_FROM_CAMERA);
        }
    }

    private void galleryAddPic() {
        if (photoFile != null){
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            File f = new File(photoFile.getAbsolutePath());
            Uri contentUri = Uri.fromFile(f);
            mediaScanIntent.setData(contentUri);
            mActivity.sendBroadcast(mediaScanIntent);
        }
    }

    private void pickPhotoFromGallery(){
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, REQUEST_PIC_FROM_GALLERY);//one can be replaced with any action code
    }

    private void openFindAddressActivity() {
        // Set the fields to specify which types of place data to
        // return after the user has made a selection.
        List<Place.Field> fields = Arrays.asList(
            Place.Field.ID,
            Place.Field.NAME,
            Place.Field.ADDRESS,
            Place.Field.LAT_LNG
        );

        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields)
                .setCountry("IT")
                .setTypeFilter(TypeFilter.ADDRESS)
            .build(requireContext());
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }
}
