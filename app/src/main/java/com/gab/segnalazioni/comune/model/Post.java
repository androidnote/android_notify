package com.gab.segnalazioni.comune.model;

import com.google.firebase.database.ServerValue;


public class Post {

    public String guid;
    public String comment;
    public String uploadedUriImage;
    public String address;
    public Object timestamp;
    public boolean starred;

    //Questo serve a Firebase per il binding automatico
    public Post(){}

    public Post(String guid, String comment, String uploadedUriImage, String address){
        this.guid = guid;
        this.comment = comment;
        this.address = address;
        this.uploadedUriImage = uploadedUriImage;
        this.timestamp = ServerValue.TIMESTAMP;
    }

}
