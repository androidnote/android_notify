package com.gab.segnalazioni.comune.ui.reports;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.gab.segnalazioni.comune.BuildConfig;
import com.gab.segnalazioni.comune.R;
import com.gab.segnalazioni.comune.model.Post;
import com.gab.segnalazioni.comune.utils.FileUtils;
import com.gab.segnalazioni.comune.utils.GlideApp;
import com.gab.segnalazioni.comune.utils.IntentShareHelper;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Locale;


public class PostReyclerAdapter extends FirebaseRecyclerAdapter<Post, PostReyclerAdapter.PostViewHolder> {

    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy - HH:mm", Locale.getDefault());
    private Context context;
    private Drawable locationDrawable;
    private Drawable dateDrawable;
    private int addressDrawablePadding;

    private ProgressDialog nDialog;

    PostReyclerAdapter(Context context, @NonNull FirebaseRecyclerOptions<Post> options) {
        super(options);
        this.context = context;

        addressDrawablePadding = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                8, context.getResources().getDisplayMetrics());

        dateDrawable = ContextCompat.getDrawable(context, R.drawable.ic_access_time_black_24dp);
        dateDrawable = DrawableCompat.wrap(dateDrawable);
        dateDrawable = dateDrawable.mutate();
        DrawableCompat.setTint(dateDrawable, ContextCompat.getColor(context, R.color.colorAccent));

        locationDrawable = ContextCompat.getDrawable(context, R.drawable.ic_location_on_white_24dp);
        locationDrawable = DrawableCompat.wrap(locationDrawable);
        locationDrawable = locationDrawable.mutate();
        DrawableCompat.setTint(locationDrawable, ContextCompat.getColor(context, R.color.colorAccent));

        nDialog = new ProgressDialog(context);
        nDialog.setMessage(context.getString(R.string.loading));
    }

    @Override
    @NonNull
    public PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(context)
                .inflate(R.layout.row_report_layout, parent, false);
        return new PostViewHolder(rowView);
    }

    @Override
    protected void onBindViewHolder(@NonNull PostViewHolder holder, int position, @NonNull Post model) {
        holder.commentTxt.setText(model.comment);

        holder.addressTxt.setText(model.address);
        holder.addressTxt.setCompoundDrawablesRelativeWithIntrinsicBounds(locationDrawable, null, null, null);
        holder.addressTxt.setCompoundDrawablePadding(addressDrawablePadding);

        holder.uploadDateTxt.setText(sdf.format(model.timestamp));
        holder.uploadDateTxt.setCompoundDrawablesRelativeWithIntrinsicBounds(dateDrawable, null, null, null);
        holder.uploadDateTxt.setCompoundDrawablePadding(addressDrawablePadding);

        Drawable starDrawable = ContextCompat.getDrawable(context, R.drawable.ic_star_white_24dp);
        starDrawable = DrawableCompat.wrap(starDrawable);
        starDrawable = starDrawable.mutate();

        if (model.starred){
            DrawableCompat.setTint(starDrawable, ContextCompat.getColor(context, android.R.color.holo_green_light));
        } else {
            DrawableCompat.setTint(starDrawable, ContextCompat.getColor(context, android.R.color.black));
        }
        holder.starredIcon.setImageDrawable(starDrawable);

        //image recovering
        StorageReference imageRef = FirebaseStorage.getInstance().getReference(model.uploadedUriImage);

        GlideApp.with(context)
                .load(imageRef)
                .placeholder(R.drawable.ic_placeholder)
                .into(holder.imageView);

        holder.shareIcon.setOnClickListener(v -> {
            try {
                nDialog.show();
                File tempFile = FileUtils.createImageFile(context);
                imageRef.getFile(tempFile)
                .addOnSuccessListener(taskSnapshot -> {
                    IntentShareHelper.shareReport((AppCompatActivity) context, model.comment,
                            FileProvider.getUriForFile(context, BuildConfig.FILE_PROVIDER_AUTHORITY, tempFile));
                    nDialog.dismiss();
                }).addOnFailureListener(e -> {
                    Toast.makeText(context.getApplicationContext(), "Errore download immagine da condividere",
                            Toast.LENGTH_SHORT).show();
                    nDialog.dismiss();
                });
            } catch (IOException e) {
                e.printStackTrace();
                nDialog.dismiss();
            }
        });
    }

    static class PostViewHolder extends RecyclerView.ViewHolder {

        AppCompatTextView uploadDateTxt, commentTxt, addressTxt;
        AppCompatImageView imageView, shareIcon, starredIcon;

        PostViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.row_post_image);
            imageView.setClipToOutline(true);
            uploadDateTxt = itemView.findViewById(R.id.row_post_date);
            commentTxt = itemView.findViewById(R.id.row_post_comment);
            addressTxt = itemView.findViewById(R.id.row_post_address);
            shareIcon = itemView.findViewById(R.id.row_post_share);
            starredIcon = itemView.findViewById(R.id.row_post_starred);
        }
    }

}
