package com.gab.segnalazioni.comune.ui.reports;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.common.ChangeEventType;
import com.firebase.ui.database.ChangeEventListener;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.gab.segnalazioni.comune.R;
import com.gab.segnalazioni.comune.model.Post;
import com.gab.segnalazioni.comune.utils.ContentLoadingProgressBar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Activity per visualizzare i post presenti nel database collegati all'utente loggato
 */
public class ReportListActivity extends AppCompatActivity {

    @BindView(R.id.post_activity_list)
    RecyclerView mPostRecyclerView;

    @BindView(R.id.progress_bar)
    ContentLoadingProgressBar mProgressBar;

    @BindView(R.id.post_activity_list_no_data)
    TextView mNoContentTxt;

    PostReyclerAdapter mPostRecyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_list);
        ButterKnife.bind(this);

        DatabaseReference db = FirebaseDatabase.getInstance().getReference();

        Query query = db
                .child("users")
                .child(FirebaseAuth.getInstance().getUid())
                .child("posts");

        FirebaseRecyclerOptions<Post> options = new FirebaseRecyclerOptions.Builder<Post>()
                .setLifecycleOwner(this)
                .setQuery(query, Post.class)
                .build();

        mProgressBar.show();

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mPostRecyclerView.setLayoutManager(layoutManager);

        mPostRecyclerViewAdapter = new PostReyclerAdapter(this, options);
        mPostRecyclerViewAdapter.getSnapshots().addChangeEventListener(new ChangeEventListener() {
            @Override
            public void onChildChanged(@NonNull ChangeEventType type, @NonNull DataSnapshot snapshot,
                                       int newIndex, int oldIndex) {
            }

            @Override
            public void onDataChanged() {
                mProgressBar.hide();
                if (mPostRecyclerViewAdapter.getItemCount() == 0){
                    mNoContentTxt.setVisibility(View.VISIBLE);
                } else {
                    mNoContentTxt.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(@NonNull DatabaseError databaseError) {
                mProgressBar.hide();
            }
        });
        mPostRecyclerView.setAdapter(mPostRecyclerViewAdapter);
    }
}
