package com.gab.segnalazioni.comune.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.gab.segnalazioni.comune.R;
import com.gab.segnalazioni.comune.notifications.FcmTokenUtils;
import com.gab.segnalazioni.comune.ui.home.MainActivity;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    private static final int RC_SIGN_IN = 123;

    @BindView(R.id.activity_login_container)
    View mRootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null) {
            registerTokenAndGoToHome();
        } else {
            // lanciamo l'activity di autenticazione di Firebase, configuriamo
            // i provider che vogliamo abilitare per l'autenticazione della nostra
            // applicazione
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(Arrays.asList(
                                    new AuthUI.IdpConfig.EmailBuilder().build(),
                                    new AuthUI.IdpConfig.GoogleBuilder().build()))
                            .setTheme(R.style.LoginStyle)
                            .setLogo(R.drawable.ic_placeholder)
                            .setIsSmartLockEnabled(false, true) //DEV only
                            .build(),
                    RC_SIGN_IN);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // controlliamo che il codice di richiesta sia quello passato in precedenza
        if (requestCode == RC_SIGN_IN) {
            // login riuscito con successo, registriamo il token e andiamo
            // nella main activity
            if (resultCode == RESULT_OK) {
                registerTokenAndGoToHome();
            } else {
                // leggiamo il valore di errore se presente
                IdpResponse response = IdpResponse.fromResultIntent(data);
                //
                if (response == null) {
                    // non ci sono dati di risposta
                    showSnackbar(R.string.sign_in_cancelled);
                } else if (response.getError().getErrorCode() == ErrorCodes.NO_NETWORK) {
                    showSnackbar(R.string.no_internet_connection);
                } else if (response.getError().getErrorCode() == ErrorCodes.UNKNOWN_ERROR) {
                    showSnackbar(R.string.unknown_error);
                } else {
                    showSnackbar(R.string.unknown_sign_in_response);
                }
            }
        }
    }

    private void registerTokenAndGoToHome(){
        // se l'utente è loggato, allora registriamo nuovamente il token
        // per le push notification, almeno siamo sicuri che è sempre il più
        // aggiornato possibile
        FcmTokenUtils.sendRegistrationToServer(this);
        // procediamo con l'activity principale
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
    }

    private void showSnackbar(@StringRes int message){
        Snackbar.make(mRootView, message, Snackbar.LENGTH_SHORT).show();
    }
}
