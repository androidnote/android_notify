package com.gab.segnalazioni.comune.ui.home;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.gab.segnalazioni.comune.R;
import com.gab.segnalazioni.comune.model.Post;
import com.gab.segnalazioni.comune.utils.KeyboardHelper;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class SendFragment extends Fragment {

    private static final String PARAM_PHOTO_URI = "photo_uri";
    private static final String PARAM_ADDRESS = "photo_address";

    public static SendFragment newInstance(Uri selectedPhoto, String address){
        SendFragment sf = new SendFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(PARAM_PHOTO_URI, selectedPhoto);
        bundle.putString(PARAM_ADDRESS, address);
        sf.setArguments(bundle);
        return sf;
    }

    private FragmentActivity mActivity;

    @BindView(R.id.send_frag_input_text)
    AppCompatEditText mCommentText;

    @BindView(R.id.send_frag_image)
    AppCompatImageView mImageView;

    @BindView(R.id.send_frag_address_text)
    AppCompatTextView mAddressView;

    private Unbinder unbinder;

    private UploadTask uploadTask;

    private Uri photoUri;
    private String address;

    private ProgressDialog nDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_send, container, false);
        unbinder = ButterKnife.bind(this, view);

        Bundle bundle = getArguments();
        if (bundle != null){
            photoUri = bundle.getParcelable(PARAM_PHOTO_URI);
            mImageView.setImageURI(photoUri);

            address = bundle.getString(PARAM_ADDRESS);
            mAddressView.setText(address);
        }

        nDialog = new ProgressDialog(mActivity);
        nDialog.setMessage(mActivity.getString(R.string.loading));

        return view;
    }

    @OnClick(R.id.send_frag_icon_send)
    public void onSendIconClick(){
        String comment = mCommentText.getText().toString();
        if (comment.length() < 10){
            Toast.makeText(mActivity, R.string.error_min_num_chars_to_send_not_reached,
                    Toast.LENGTH_SHORT).show();
        } else {
            KeyboardHelper.hideKeyboard(mActivity);

            nDialog.show();

            FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
            StorageReference storageRef = firebaseStorage.getReference();
            StorageReference imageRef = storageRef.child("images/" + photoUri.getLastPathSegment());

            StorageMetadata metadata = new StorageMetadata.Builder()
                    .setContentType("image/jpeg")
                    .build();

            uploadTask = imageRef.putFile(photoUri, metadata);

            // Register observers to listen for when the download is done or if it fails
            uploadTask.addOnFailureListener(exception -> {
                // Handle unsuccessful uploads
                Toast.makeText(mActivity, "error", Toast.LENGTH_SHORT).show();
                exception.printStackTrace();
                nDialog.dismiss();
            }).addOnProgressListener(taskSnapshot -> {
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                System.out.println("Upload is " + progress + "% done");
            }).addOnSuccessListener(taskSnapshot -> {
                //now save the new object for this user
                FirebaseAuth auth = FirebaseAuth.getInstance();
                if (auth.getCurrentUser() != null) {

                    DatabaseReference db = FirebaseDatabase.getInstance().getReference();

                    //get the key
                    String key = db.child("users")
                            .child(auth.getUid())
                            .child("posts")
                            .push()
                            .getKey();

                    //create the object with the same key
                    db.child("users")
                        .child(auth.getUid())
                        .child("posts")
                        .child(key)
                        .setValue(new Post(key, comment, taskSnapshot.getMetadata().getPath(), address))
                        .addOnSuccessListener(aVoid -> {
                            //everything is completed
                            Toast.makeText(mActivity, R.string.report_sent, Toast.LENGTH_SHORT).show();

                            nDialog.dismiss();
                            //Torniamo al menu iniziale, rimuovendo tutti i fragment tranne quello iniziale
                            while(mActivity.getSupportFragmentManager().getBackStackEntryCount() > 0){
                                mActivity.getSupportFragmentManager().popBackStackImmediate();
                            }
                        })
                        .addOnFailureListener(e -> {
                            nDialog.dismiss();
                            e.printStackTrace();
                        });
                } else {
                    imageRef.delete()
                            .addOnSuccessListener(command -> {
                                //image upload has been rolled back
                            })
                            .addOnFailureListener(e ->  e.printStackTrace());
                }
            });
        }
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (FragmentActivity) context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        nDialog.dismiss();
    }

    @Override
    public void onStop() {
        if (uploadTask != null){
            uploadTask.cancel();
        }
        super.onStop();
    }
}
