package com.gab.segnalazioni.comune;

import android.app.Application;

import com.google.android.libraries.places.api.Places;

/**
 * Created by Gabriele Coppola on 25/09/2019.
 */
public class AppApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Places.initialize(this, "AIzaSyCrs58GuujXt78Xvb2nmnHGzLBqElZTIAM");
    }
}
