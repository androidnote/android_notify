package com.gab.segnalazioni.comune.notifications;

import android.content.Context;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;


public class FcmTokenUtils {

    public static void sendRegistrationToServer(Context context) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null) {
            // chiediamo il token a firebaseInstanceId
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            // prendiamo la referenza al db dell'attuale progetto firebase
            DatabaseReference db = FirebaseDatabase.getInstance().getReference();
            // partendo dalla root del database, scendiamo nell'albero con child
            // fino a trovare la proprietà token, a cui vogliamo impostare il valore
            // ricevuto in refreshedToken
            db.child("users").child(auth.getUid()).child("token").setValue(refreshedToken)
                    .addOnSuccessListener(aVoid -> {
                        Toast.makeText(context, "Token FCM registrato/aggiornato", Toast.LENGTH_SHORT).show();
                    })
                    .addOnFailureListener(e -> {
                        Toast.makeText(context, "Errore registrazione token FCM", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                        //proviamo a rimandarlo, magari era un problema di connessione
                        //(sarebbe da gestirlo meglio con un numero di tentativi massimi)
                        sendRegistrationToServer(context);
                    });
        }
    }

}
