import { ListComponent } from './list/list.component';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { AppComponent } from './app.component';

import { AuthGuard } from './auth-guard.service';

const appRoutes: Routes = [
    {
      path: 'login',
      component: LoginComponent
    },
    {
      path: 'list',
      canActivate: [AuthGuard],
      component: ListComponent
    },
    { path: '**', redirectTo: 'list' }
];

export const AppRoutes = RouterModule.forRoot(appRoutes);