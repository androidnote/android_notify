import { Component, OnInit, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  signupForm: FormGroup;

  loginError: boolean = false;

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) {
    this.signupForm = this.fb.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]]
    });
  }

  get password(){
    return this.signupForm.get('password');
  }

  get email(){
    return this.signupForm.get('email');
  }

  ngOnInit() {
    if (this.authService.isLoggedIn()){
      this.router.navigate(['list'])
    }
  }

  onLoginWithGoogle(){
    this.authService.signInWithGoogle()
        .then(result => {
          console.log(result);
      }).catch(error => {
          console.log(error);
          this.loginError = true;
          //show the error only for 2 seconds
          setTimeout(() => {
            this.loginError = false;
          }, 3000)
      });
  }

  onSubmit() {
    if (this.password.valid && this.email.valid){
      this.authService.signInRegular(this.email.value, this.password.value)
        .then(result => {
          console.log(result);
      }).catch(error => {
          console.log(error);
          this.loginError = true;
          //show the error only for 2 seconds
          setTimeout(() => {
            this.loginError = false;
          }, 3000)
      });
    }
  }

}
