export interface Post {
  guid: string;
  starred: boolean;
  address: string;
  comment?: string;
  timestamp: number;
  uploadedUriImage: string;
  fullImageUrl: string;
}