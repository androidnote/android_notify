import { Post } from "./post";

export class AppUser {
    userId: string;
    posts: Post[] = [];
}