import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = "Login";

  // il nome dell'utente loggato
  name: string;

  constructor(public authService: AuthService, private router: Router) {
    // sottoscriviamoci all'userDetails, quando l'utente verrà loggato
    // viene chiamato questo sottoscrittore e a quel punto name
    // verrà valorizzato
    this.authService.userUpdated$.subscribe((user: firebase.User) => {
      this.name = user.displayName;
    }); 
  }

}
