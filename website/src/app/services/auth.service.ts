import { AngularFireAuth } from "angularfire2/auth";
import { User } from '@firebase/auth-types';
import { Output, Injectable } from "@angular/core";
import { Router } from "@angular/router";

import * as firebase from 'firebase/app';
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";

@Injectable()
export class AuthService {

    // l'utente loggato attualmente
    private _user: firebase.User;
    // il subject per inviare aggiornamenti sulle modifiche dell'utente attuale
    public userUpdated$: Subject<firebase.User> = new Subject<firebase.User>();
    
    constructor(private _firebaseAuth: AngularFireAuth, private router: Router){
      _firebaseAuth.authState.subscribe((user) => {
        // l'autenticazione di firebase ha tornato un user, se non è null
        // salviamo l'user all'interno del servizio
        if (user) {
          this._user = user;
          // mandiamo il valore dell'user ha tutti i sottoscrittori
          this.userUpdated$.next(user);
          // andiamo alla lista delle segnalazioni 
          this.router.navigate(['/list'])
        } else {
          this._user = null;
        }
      });
    }

    isLoggedIn() {
      if (this._user == null) {
        return false;
      } else {
        return true;
      }
    }

    getUserUid(): string | null {
      if (this.isLoggedIn()){
        return this._user.uid;
      } else {
        return null;
      }
    }

    logout() {
        this._firebaseAuth.auth.signOut()
        .then((res) => this.router.navigate(['/login']));
    }

    signInWithGoogle(): Promise<any> {
      var provider = new firebase.auth.GoogleAuthProvider();
      provider.setCustomParameters({ prompt: 'select_account' });
      return this._firebaseAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
    }

    signInRegular(email, password): Promise<any> {
      const credential = firebase.auth.EmailAuthProvider.credential( email, password );
      return this._firebaseAuth.auth.signInWithCredential(credential)
    }

}
