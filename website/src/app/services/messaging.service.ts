import { Injectable } from "@angular/core";
import * as firebase from 'firebase';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Post } from '../data/post';

@Injectable()
export class MessagingService {
    
    message = {
        notification : {
          title : "Aggiornamenti alla tua segnalazione",
          body: "REPLACE AT RUNTIME"
        },
        priority: 'high',
        to: "REPLACE AT RUNTIME"
    };
    
    constructor(private http: HttpClient){}

    //La chiave in Authorization rimane fissa per il progetto finché qualcuno non la resetta, da non modificare
    httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': 'key=AAAARw5U_wU:APA91bG-KvMocU-mtxj_rG-s-UP4N5YjGQoMpabRnom0nLrstrdy_aU18xBiLJUCGff8tU2oqKUtw947fk2UsJa7ZjuIcOgFV2Stc5MSGX-JAZckEyt8DDXzQtZIgT_-AMwGn15J6NGe'
        })
    };

    //Url base per Firebase (versione legacy, senza l'utilizzo dell'Admin SDK)
    FIREBASE_BASE_URL_FCM = "https://fcm.googleapis.com/fcm/send";

    //Invia una notifica al server di Firebase (che poi la rimbalza sul dispositivo registrato con il token passato)
    sendMessage(fcmToken: string, post: Post){
        this.message.to = fcmToken;
        this.message.notification.body = "La segnalazione a " + post.address + " é stata presa in considerazione";
        return this.http.post(this.FIREBASE_BASE_URL_FCM, this.message, this.httpOptions);
    }
}