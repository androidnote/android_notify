import { Validators } from '@angular/forms';
import { Injectable } from "@angular/core";
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireStorage } from 'angularfire2/storage';
import { Post } from '../data/post';
import { Observable } from "rxjs/Observable";
import { concatMap, delay, mergeMap, flatMap, switchMap } from 'rxjs/operators';
import { AppUser } from "../data/user";

@Injectable()
export class DataService {

    imageRefCache: Map<string, string> = new Map<string, string>();

    constructor(private db: AngularFireDatabase, private storage: AngularFireStorage) {}

    getPostsForUserId(userId: string) : Observable<AppUser[]> {
        let items$ = this.db.list('users/' + userId + '/posts').valueChanges() as Observable<Post[]>;
        let appUsers: AppUser[] = [];
        return items$.map(posts => {
            //per ogni oggetto tornato da Firebase, andiamo a recuperare l'immagine collegata
            posts.forEach(post => {
                //prendiamo l'URI relativo (senza mettere la cartella images/)
                const ref = this.storage.ref(post.uploadedUriImage);
                for(let key of Array.from(this.imageRefCache.keys()) ) {
                  if (key === post.uploadedUriImage) {
                    post.fullImageUrl = this.imageRefCache.get(key);
                    return post;
                  }
                }

                ref.getDownloadURL().subscribe(fullUrl => {
                    //fullUrl ora contiene il path assoluto per l'immagine da caricare
                    post.fullImageUrl = fullUrl;
                    this.imageRefCache.set(post.uploadedUriImage, fullUrl);
                })
            });
            return appUsers;
        });
    };

    isUserAdmin(userId): Observable<boolean> {
      return this.db.object('users')
      .valueChanges()
      .map(userMap => {
        if (userMap.hasOwnProperty(userId)) {
          if (userMap[userId]['admin']){
            return true;
          }
        }
        return false;
      });
    }

    getAllPosts(): Observable<AppUser[]> {
      return this.db.object('users')
      .valueChanges()
      .map(userMap => {
        let appUserList: AppUser[] = [];
        let userWithPosts: boolean = false;
        //cicliamo nella mappa degli utenti senza conoscere la chiave
        for (let userId in userMap) {
          //creiamo un nuovo oggetto di tipo AppUser
          let appUser: AppUser = new AppUser();
          if (userMap.hasOwnProperty(userId)) {
            //salviamo l'userId corrente
            appUser.userId = userId;
            //prendiamo i post di questo utente
            let postsMap = userMap[userId].posts;
            //cicliamo nella mappa dei posts senza conoscere la chiave
            for (let postId in postsMap) {
              if (postsMap.hasOwnProperty(postId)) {
                //prendiamo un post alla volta e lo salviamo nei post dell'utente
                let post: Post = postsMap[postId];
                appUser.posts.push(post);
                userWithPosts = true;
                //prendiamo l'URI relativo (senza mettere la cartella images/)
                const ref = this.storage.ref(post.uploadedUriImage);
                for(let key of Array.from(this.imageRefCache.keys()) ) {
                    if (key === post.uploadedUriImage) {
                        post.fullImageUrl = this.imageRefCache.get(key);
                    }
                }
                ref.getDownloadURL().subscribe(fullUrl => {
                    //fullUrl ora contiene il path assoluto per l'immagine da caricare
                    post.fullImageUrl = fullUrl;
                    this.imageRefCache.set(post.uploadedUriImage, fullUrl);
                })
              }
            }
          }
          //aggiungiamo l'array solamente se abbiamo trovato almeno un POST per questo utente
          if (userWithPosts) {
            appUserList.push(appUser);
          }
          userWithPosts = false;
        }
        return appUserList;
      });
    }

    getFcmTokenForUserId(userId: string): Observable<string>{
      return this.db.object('users/' + userId + '/token').snapshotChanges().map(res => {
          return res.payload.val();
      });
    }

    updatePostStarStatus(userId: string, post: Post): Promise<any> {
      return this.db.database.ref('users/' + userId + '/posts/' + post.guid).update({"starred": post.starred});
    }

}