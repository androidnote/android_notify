import { MessagingService } from './../services/messaging.service';
import { AuthService } from './../services/auth.service';
import { Observable } from 'rxjs/Observable';
import { DataService } from './../services/data.service';
import { Component, OnInit } from '@angular/core';
import { Post } from '../data/post';
import { AppUser } from '../data/user';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent {

  usersPost: AppUser[] = [];
  isAdmin: boolean;

  constructor(
    private dataService: DataService, 
    private authService: AuthService,
    private messageService: MessagingService) {
    if (this.authService.isLoggedIn()){
      this.loadPosts(this.authService.getUserUid());
    }
  }

  loadPosts(userId?: string){
    if (userId != null){
      this.dataService.isUserAdmin(userId).subscribe((isAdmin: boolean) => {
        //salviamo lo stato corrente dell'admin
        this.isAdmin = isAdmin;
        this.dataService.getAllPosts().subscribe((users: AppUser[]) => {
          this.usersPost = isAdmin ? users : users.filter(user => user.userId === userId);
        });
      });
      
    } else {
      this.usersPost = [];
    }
  }

  toggleState(userId: string, post: Post){
    if (!this.isAdmin) {
      //un non amministratore non puó effettuare questa operazione
      alert('Solo gli amministratori possono modificare questa impostazione');
      return;
    }

    if (post.starred){
      post.starred = false
    } else {
      post.starred = true
    }
    
    this.dataService.updatePostStarStatus(userId, post).then(result => {
      //Invia la notifica al server (che poi la rimanda verso i dispositivi connessi)
      this.dataService.getFcmTokenForUserId(userId).subscribe(fcmToken => {
        this.messageService.sendMessage(fcmToken, post).subscribe(() => {
          console.log("Notifica push inviata");
        });
      });
    }).catch(error => {
      console.log(error);
    });
  }

}
